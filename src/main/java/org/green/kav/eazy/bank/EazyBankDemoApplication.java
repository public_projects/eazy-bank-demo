package org.green.kav.eazy.bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
//@EnableJpaRepositories("org.green.kav.eazy.bank.repository") // not necessary
//@EntityScan("org.green.kav.eazy.bank.model") // not necessary
public class EazyBankDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EazyBankDemoApplication.class, args);
	}

}
