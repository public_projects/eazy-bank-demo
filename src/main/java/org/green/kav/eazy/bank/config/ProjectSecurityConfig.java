package org.green.kav.eazy.bank.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import javax.sql.DataSource;

@Configuration
public class ProjectSecurityConfig {
    public final static String ACCOUNT_URL = "/myAccount";
    public final static String BALANCE_URL = "/myBalance";
    public final static String LOAN_URL = "/myLoans";
    public final static String CARD_URL = "/myCards";
    public final static String NOTICE_URL = "/notices";
    public final static String CONTACT_URL = "/contact";

    @Bean
    SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        // example of configuring the CSRF
        http.csrf(csrf -> csrf.ignoringRequestMatchers("/register"));

        http.authorizeHttpRequests((requests) -> {
            requests.requestMatchers(ACCOUNT_URL, BALANCE_URL, LOAN_URL, CARD_URL).authenticated()
                    .requestMatchers(NOTICE_URL, CONTACT_URL, "/register").permitAll();
        });
        http.formLogin(Customizer.withDefaults());
        http.httpBasic(Customizer.withDefaults());
        return http.build();
    }

//    @Bean
//    public InMemoryUserDetailsManager userDetailsManager() {

    /**
     * Approach 1: we use withDefaultPasswordEncoder() method while creating the user details.
     */
//        UserDetails admin = User.withDefaultPasswordEncoder()
//                .username("user1")
//                .password("password")
//                .roles("admin")
//                .build();
//
//        UserDetails user = User.withDefaultPasswordEncoder()
//                .username("user2")
//                .password("password")
//                .roles("read")
//                .build();
//
//        return new InMemoryUserDetailsManager(admin, user);

    /**
     * Approach 2: where we use withDefaultPasswordEncoder() method with creating the user details.
     */
//        UserDetails admin = User.withUsername("user1")
//                .password("password")
//                .roles("admin")
//                .build();
//
//        UserDetails user = User.withDefaultPasswordEncoder()
//                .username("user2")
//                .password("password")
//                .roles("read")
//                .build();
//
//        return new InMemoryUserDetailsManager(admin, user);
//    }
//    @Bean
//    public UserDetailsService userDetailsService(DataSource dataSource) {
//        return new JdbcUserDetailsManager(dataSource);
//    }

    /**
     * NoOpPasswordEncoder is not recommended for production usage.
     * Use only for non-prod.
     *
     * @return
     */
//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        return NoOpPasswordEncoder.getInstance();
//    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
